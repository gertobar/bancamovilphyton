from analisis.views import SolicitudViewSet
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    path('api/', SolicitudViewSet.as_view()),
    path('api/<int:pk>/', SolicitudViewSet.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
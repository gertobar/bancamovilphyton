from rest_framework import serializers
from .models import Solicitud


class SolicitudSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Solicitud
        fields = '__all__'
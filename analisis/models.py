from django.db import models

# Create your models here.


class Solicitud(models.Model):
    
    plazo_meses = models.IntegerField(null=False, blank=False)
    proposito_credito = models.CharField(max_length=51)
    monto_credito = models.IntegerField(null=False, blank=False)
    tipo_empleado = models.CharField(max_length=50)
    estado_civil = models.CharField(max_length=50)
    avaluo_vivienda = models.IntegerField(null=False, blank=False)
    activos = models.CharField(max_length=50)
    vivienda = models.CharField(max_length=50)
    empleo = models.CharField(max_length=50)
    trabajador_extranjero = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.estado

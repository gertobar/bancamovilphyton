from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Solicitud
from .serializers import SolicitudSerializers
# Create your views here.

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier

class SolicitudViewSet(APIView):
    queryset = Solicitud.objects.all()
    serializer_class = SolicitudSerializers

    def post(self, request):
        response = ""
        serializer = SolicitudSerializers(data = request.data)
        # serializer.is_valid(raise_exception=True)
        if(serializer.is_valid()):
            plazo_meses = request.data['plazo_meses']
            proposito_credito = request.data['proposito_credito']
            monto_credito = request.data['monto_credito']
            tipo_empleado = request.data['tipo_empleado']
            estado_civil = request.data['estado_civil']
            avaluo_vivienda = request.data['avaluo_vivienda']
            activos = request.data['activos']
            vivienda = request.data['vivienda']
            empleo = request.data['empleo']
            trabajador_extranjero = request.data['trabajador_extranjero']
            
            df = pd.read_csv('dataset.csv', sep=';')
            df.drop(['DNI', 'HISTORIALCREDITO', 'SALDOCUENTAAHORROS', 'TASAPAGO', 'GARANTE', 'EDAD', 'CANTIDADCREDITOSEXISTENTES'],  axis='columns', inplace=True)
            scaler = StandardScaler()
            df.iloc[:, 1]= df.iloc[:, 1].astype('category').cat.codes
            df.iloc[:, 3] = df.iloc[:, 3].astype('category').cat.codes
            df.iloc[:, 4] = df.iloc[:, 4].astype('category').cat.codes
            df.iloc[:, 6] = df.iloc[:, 6].astype('category').cat.codes
            df.iloc[:, 7] = df.iloc[:, 7].astype('category').cat.codes
            df.iloc[:, 8] = df.iloc[:, 8].astype('category').cat.codes
            df.iloc[:, 9] = df.iloc[:, 9].astype('category').cat.codes
            x = df.iloc[:, 0:10].values
            y = df.iloc[:, 10:11].values
            x = scaler.fit_transform(x)
            encoder = OneHotEncoder()
            y = encoder.fit_transform(y).toarray()
            X_train, X_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, stratify = y)
            mlp = MLPClassifier(solver='lbfgs',  activation='relu', alpha=0.001, max_iter=8000,
                   hidden_layer_sizes=(5, 2))
            mlp.fit(X_train, y_train)
            prediccion = mlp.predict(X_test)
            precision = accuracy_score(prediccion, y_test)
            print('Precision ', (precision*100))

            predecir = [[plazo_meses, proposito_credito, monto_credito, tipo_empleado, estado_civil, avaluo_vivienda, activos, vivienda, empleo, trabajador_extranjero]]
            if((precision*100) >= 70):
                response = "Aprobado"
            else:
                response = "Denegado"
            
            # predecir = pd.DataFrame(predecir)
            # predecir.iloc[:, 1]= predecir.iloc[:, 1].astype('category').cat.codes
            # predecir.iloc[:, 3] = predecir.iloc[:, 3].astype('category').cat.codes
            # predecir.iloc[:, 4] = predecir.iloc[:, 4].astype('category').cat.codes
            # predecir.iloc[:, 6] = predecir.iloc[:, 6].astype('category').cat.codes
            # predecir.iloc[:, 7] = predecir.iloc[:, 7].astype('category').cat.codes
            # predecir.iloc[:, 8] = predecir.iloc[:, 8].astype('category').cat.codes
            # predecir.iloc[:, 9] = predecir.iloc[:, 9].astype('category').cat.codes
            # val = np.array([predecir])
            # pos = mlp.predict(val)
            # print(pos)
            print("El usuario es :", response)
        else:
            response = "Existe algun valor vacio corrige"
        # serializer.save()
        
        return HttpResponse(response, content_type='text/plain')

